package ru.teamidea.td.b2c.storefront.cache.services.impl;

import com.mongodb.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import ru.teamidea.td.b2c.storefront.cache.services.ICacheService;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rauf_Aliev on 7/15/2016.
 */
public class MongoDbService implements ICacheService {

    private static final Logger LOG = Logger.getLogger(MongoDbService.class);

    private String dbName;
    private Mongo mongoClient;
    private String dbCollectionName;
    private DBCollection dbCollection;

    public MongoDbService(final Mongo mongoClient, final String dbName, final String dbCollectionName) {
        this.mongoClient = mongoClient;
        this.dbName = dbName;
        this.dbCollectionName = dbCollectionName;
    }

    public Map<String, String> getMap(String key) throws UnknownHostException {

        if (dbCollection == null) {
            connect();
        }

        final BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("key", key);

        final DBCursor cursor = dbCollection.find(searchQuery);

        final Map<String, String> map = new HashMap<>();
        if (cursor.hasNext()) {
            final DBObject k = cursor.next();
            for (String akey : k.keySet()) {
                map.put(akey, k.get(akey).toString());
            }
            return map;
        }
        return null;
    }

    public String get(String key) throws UnknownHostException {

        if (dbCollection == null) {
            connect();
        }

        final BasicDBObject searchQuery = new BasicDBObject();

        searchQuery.put("key", key);

        final DBCursor cursor = dbCollection.find(searchQuery);

        StringBuilder s = new StringBuilder();
        while (cursor.hasNext()) {
            final DBObject k = cursor.next();
            s.append(k.get("value"));
        }
        return s.toString();
    }

    public String put(String key, String value, Map<String, String> attributes) throws UnknownHostException {
        if (get(key).equals("")) {

            final BasicDBObject document = new BasicDBObject();
            document.put("key", key);
            document.put("value", value);

            long ctime = System.currentTimeMillis();
            document.put("ctime", ctime);

            for (String keyattr : attributes.keySet()) {
                document.put(keyattr, attributes.get(keyattr));
            }

            dbCollection.insert(document);
            dbCollection.save(document);
        }
        return value;
    }

    public boolean lookup(String key, String value) {
        final BasicDBObject query = new BasicDBObject(key, value);

        try (DBCursor cursor = dbCollection.find(query)) {
            if (cursor.hasNext()) {
                LOG.debug(cursor.next());
                return true;
            }
        }
        return false;
    }

    public void update(String key, String value) throws UnknownHostException {
        if (StringUtils.isBlank(key) || StringUtils.isBlank(value)) {
            return;
        }

        final BasicDBObject filter = new BasicDBObject("key", key);
        final BasicDBObject newValue = new BasicDBObject();
        newValue.append("value", value);
        newValue.append("ctime", System.currentTimeMillis());

        final BasicDBObject updateOperationDocument = new BasicDBObject("$set", newValue);

        if (dbCollection == null) {
            connect();
        }

        dbCollection.update(filter, updateOperationDocument);
    }

    public void removeAll(String key, String value) throws UnknownHostException {

        if (dbCollection == null) {
            connect();
        }

        final BasicDBObject query = new BasicDBObject(key, value);
        dbCollection.remove(query);
    }

    private void connect() throws UnknownHostException {

        if (StringUtils.isBlank(dbName)) {
            LOG.debug("dbName property is null. Check your local.properties file!");
            return;
        }

        final DB db = mongoClient.getDB(dbName);
        if (db == null) {
            LOG.debug("db with name " + dbName + " is null");
            return;
        }

        if (StringUtils.isBlank(dbCollectionName)) {
            LOG.debug("dbCollectionName property is null. Check your local.properties file!");
            return;
        }

        dbCollection = db.getCollection(dbCollectionName);
        if (dbCollection == null) {
            LOG.debug("collection with name " + dbCollectionName + " is null");
        }
    }
}
