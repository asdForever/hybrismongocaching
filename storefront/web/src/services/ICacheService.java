package ru.teamidea.td.b2c.storefront.cache.services;

import java.net.UnknownHostException;
import java.util.Map;

/**
 * Created by Rauf_Aliev on 7/29/2016.
 */
public interface ICacheService {

    Map<String, String> getMap(String key) throws UnknownHostException;
    String get(String key) throws UnknownHostException;
    String put(String key, String value, Map<String, String> attributes) throws UnknownHostException;
}
