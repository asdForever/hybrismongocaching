package ru.teamidea.td.b2c.storefront.cache.tags;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import ru.teamidea.td.b2c.storefront.cache.services.impl.MongoDbService;
import ru.teamidea.utils.helper.TIConfigHelper;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.DynamicAttributes;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

public class CacheTag extends BodyTagSupport implements DynamicAttributes {

    private static final Logger LOG = Logger.getLogger(CacheTag.class);

    private static final String CACHE_MONGO_DATABASE_ACTIVATION_KEY = "cache.mongo.activation";
    private static final String CACHE_MONGO_DATABASE_TTL_KEY = "cache.mongo.ttl";

    private MongoDbService mongoDBService;
    private TIConfigHelper tiConfigHelper;

    private String url;
    private String isCorporate;
    private String siteUid;
    private String currentLang;

    private Boolean isCacheActive;
    private Integer ttl;

    private Map<String, String> dynamicAttributes;

    @Override
    public void setDynamicAttribute(String s, String s1, Object o) throws JspException {
        if (dynamicAttributes == null) {
            dynamicAttributes = new HashMap<>();
        }
        if (o == null) {
            return;
        }

        if (s1.equals("ttl")) {
            this.dynamicAttributes.put("ttl", o.toString());
            ttl = Integer.parseInt(o.toString());
            return;
        }

        if (o.equals("url")) {
            final ServletRequest sr = this.pageContext.getRequest();
            final String url = ((HttpServletRequest) sr).getRequestURI();
            this.dynamicAttributes.put("url", url);

            return;
        }

        if (o.equals("url+get")) {
            final ServletRequest sr = this.pageContext.getRequest();
            final String qs = ((HttpServletRequest) sr).getQueryString();
            final String url = ((HttpServletRequest) sr).getRequestURL().toString();

            this.dynamicAttributes.put("url+get", url + "?" + qs);

            return;
        }

        if (o.equals("sessionid")) {
            final ServletRequest sr = this.pageContext.getRequest();
            final Cookie[] cookie = ((HttpServletRequest) sr).getCookies();

            for (Cookie aCookie : cookie) {
                if (aCookie.getName().equals("JSESSIONID")) {
                    System.out.println("JSESSIONID=" + aCookie.getValue());
                    this.dynamicAttributes.put("sessionid", aCookie.getValue());
                    return;
                }
            }
            return;
        }

        this.dynamicAttributes.put(s1, o.toString());
    }

    private String prepareCacheKey() {
        return getSiteUid() + "_" + getUrl() + "_" + getCurrentLang() + "_" + getIsCorporate();
    }

    private String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    private String getIsCorporate() {
        return isCorporate;
    }

    public void setIsCorporate(String isCorporate) {
        this.isCorporate = isCorporate;
    }

    public String getSiteUid() {
        return siteUid;
    }

    public void setSiteUid(String siteUid) {
        this.siteUid = siteUid;
    }

    public String getCurrentLang() {
        return currentLang;
    }

    public void setCurrentLang(String currentLang) {
        this.currentLang = currentLang;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            if (!getCacheActive()) {
                return EVAL_BODY_BUFFERED;
            }

            final String data = getDocumentValueFromCache();

            if (StringUtils.isNotBlank(data)) {
                pageContext.getOut().print(data);
                return SKIP_BODY;
            }
        } catch (Exception e) {
            LOG.error("Exception during doStartTag: " + e);
        }
        return EVAL_BODY_BUFFERED;
    }

    @Override
    public BodyContent getBodyContent() {
        return super.getBodyContent();
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            if (!getCacheActive()) {
                pageContext.getOut().print(bodyContent.getString());
                return super.doEndTag();
            }

            if (bodyContent != null && StringUtils.isNotBlank(bodyContent.getString())) {
                updateDocumentInCache(bodyContent.getString());
                updateResponseHeader();
                pageContext.getOut().print(bodyContent.getString());
            }
        } catch (Exception e) {
            LOG.error("Error during doEndTag: " + e);
        }

        release();
        return super.doEndTag();
    }

    @Override
    public int doAfterBody() throws JspException {
        return super.doAfterBody();
    }

    private String getDocumentValueFromCache() throws UnknownHostException {
        final Map<String, String> datamap = getMongoDBService().getMap(prepareCacheKey());

        if (datamap != null && !isDocumentExpired(datamap.get("ctime"))) {
            return datamap.get("value");
        } else {
            return null;
        }
    }

    private boolean isDocumentExpired(final String documentTimeStr) {
        return StringUtils.isBlank(documentTimeStr) || System.currentTimeMillis() > Long.parseLong(documentTimeStr) + getTtl() * 1000;
    }

    private void updateResponseHeader() {
        final String prevValueStr = ((HttpServletResponse) this.pageContext.getResponse()).getHeader("X-Cache-Control");
        if (StringUtils.isNotBlank(prevValueStr)) {
            int prevValue = Integer.parseInt(prevValueStr);
            int newValue = getTtl();
            if (newValue < prevValue) {
                ((HttpServletResponse) this.pageContext.getResponse()).setHeader("X-Cache-Control", newValue + "");
            }
        } else {
            ((HttpServletResponse) this.pageContext.getResponse()).setHeader("X-Cache-Control", getTtl() + "");
        }
        ((HttpServletResponse) this.pageContext.getResponse()).setHeader("Cache-Control", "public, max-age:10");
    }

    private void updateDocumentInCache(String documentValue) throws UnknownHostException {
        final String key = prepareCacheKey();
        final Map<String, String> data = getMongoDBService().getMap(key);
        if (data == null) {
            getMongoDBService().put(key, documentValue, getDynamicAttributes());
        } else {
            getMongoDBService().update(key, documentValue);
        }
    }

    private MongoDbService getMongoDBService() {
        if (mongoDBService == null) {
            mongoDBService = (MongoDbService) de.hybris.platform.core.Registry.getApplicationContext().getBean("pageCacheService");
        }
        return mongoDBService;
    }

    private TIConfigHelper getTiConfigHelper() {
        if (tiConfigHelper == null) {
            tiConfigHelper = (TIConfigHelper) de.hybris.platform.core.Registry.getApplicationContext().getBean("tiConfigHelper");
        }
        return tiConfigHelper;
    }

    private Boolean getCacheActive() {
        if (isCacheActive == null) {
            isCacheActive = Boolean.valueOf(getTiConfigHelper().getPropertyValue(CACHE_MONGO_DATABASE_ACTIVATION_KEY));
        }
        return isCacheActive;
    }

    private Integer getTtl() {
        if (ttl == null || ttl == 0) {
            ttl = Integer.valueOf(getTiConfigHelper().getPropertyValue(CACHE_MONGO_DATABASE_TTL_KEY));
            if (ttl == null || ttl == 0) {
                ttl = 5;
            }
        }
        return ttl;
    }

    private Map<String, String> getDynamicAttributes() {
        if (dynamicAttributes == null) {
            dynamicAttributes = new HashMap<>();
        }
        return dynamicAttributes;
    }
}