<%@ page trimDirectiveWhitespaces="true" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cache" uri="/WEB-INF/common/tld/cacheTag.tld" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize ifAllGranted="ROLE_CORPORATESALES">
    <c:set var="corporate" value="true"/>
</sec:authorize>

<c:if test="${(corporate==null || corporate==false) && (byodPage==false || byodPage==null)}">
    <c:set var="corporate" value="false"/>
</c:if>

<template:page pageTitle="${pageTitle}">
	<div class="tda-wrapper">
        <div class="left-panel tda-leftmenu">
            <cache:cached url="homepageNavigationBar" isCorporate="${corporate}" siteUid="${siteUid}" currentLang="${currentLang}">
                <cms:pageSlot position="NavigationBar" var="component">
                    <cms:component component="${component}" element="div" class="menu_indexpage tda-leftmenu__menucatalog"/>
                </cms:pageSlot>
            </cache:cached>
        </div>
    </div>
</template:page>